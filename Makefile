# This must be the first this in Makefile.common
TOP := $(dir $(lastword $(MAKEFILE_LIST)))

all: clean-doc

clean: clean-doc
	rm -rf *.fasl

clean-doc:
	rm -f *.log *.aux *.tex *.out *.dvi *.toc

fresh: clean
	rm -rf *.pdf
