# uber-larp

These are some Common Lisp tools I used to help create a fun [Uber Larp](http://www.drivethrurpg.com/product/108971/ber-LARP--Steampunk) Steampunk live action role playing game, the [Airship Levi](https://gitlab.com/file13/uber-larp/raw/master/larp/airship-levi/steampunk-levi.pdf)

The first part of the code is simply some tools for doing rock paper scissors with a little simple AI.  I never actually used them in the game, but it was fun to write and play with.  The second part consists of tools to generate LaTex code for character sheets.  The other stuff was generated with Open Office.

Good times!

## License

Public domain.  Have fun!