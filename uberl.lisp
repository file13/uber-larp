#|

uberl.lisp
Michael Rossi
Mon Dec  8 12:06:32 CST 2014

This was something I hacked together as tools for the Uber Larp steampunk game, 
though these tools could be used with any Larp that uses the same basic rules.

If you just want a random outcome without the player picking rock, paper, or
scissors, then use auto-check. If they want to choice thier choice, use
quick-check.

Otherwise the basic challenge function does the work. It has an optional smart
setting, which if set, has the computer make a weighted choice based on the
player's history. Ideally, we'd want to record these choices for each player,
but for now, they're global.

|#

;;; Basic rock-paper scissors tools
(defparameter *rps* '(rock paper scissors)
  "A list of values for playing rock paper scissors.")

(defun rps-p (value)
  "Predicate to check if 'VALUE' is a valid rock-paper-scissors value."
  (member value *rps*))

(defun rps ()
  "Returns a random rock-paper-scirros value."
  (random-elt *rps*))

;;; Scoring tools. Should be self-explanatory.
(defparameter *computer-wins* 0)
(defparameter *player-wins* 0)
(defparameter *total-games* 0)

(defun score-tie () (incf *total-games*))
(defun score-player () (incf *total-games*) (incf *player-wins*))
(defun score-computer () (incf *total-games*) (incf *computer-wins*))
(defun reset-score ()
  (setf *computer-wins* 0
        *player-wins*   0
        *total-games*   0))

(defun print-score ()
  (format t "Total games: ~A~%" *total-games*)
  (format t "Player wins: ~A~%" *player-wins*)
  (format t "Computer wins: ~A~%" *computer-wins*))

(defun rps-winner (computer human &key (keep-score t))
  "Determines the winner of a rock-paper-scissors game."
  (assert (and (rps-p computer) (rps-p human)))
  (let ((order '(scissors paper rock scissors)))
    (cond ((eq computer human)
           (if keep-score (score-tie))
           'tie)
          ((eq (second (member computer order)) human) 
           (if keep-score (score-computer))
           'computer)
          (t 
           (if keep-score (score-player))
           'human))))

;;; Weighted distributions
(defparameter *player-history* (make-hash-table) 
  "Hash to record the player's choices.")

(defun history ()
  "Returns the players history as an alist."
  (alexandria:hash-table-alist *player-history*))

(defun reset-history ()
  "Resets the players history to equal values."
  (setf (gethash 'rock *player-history*) 1)
  (setf (gethash 'paper *player-history*) 1)
  (setf (gethash 'scissors *player-history*) 1))

(defun add-history (rps-choice)
  "Updates the player's latest choice."
  (assert (rps-p rps-choice))
  (setf (gethash rps-choice *player-history*) 
        (1+ (gethash rps-choice *player-history*))))

(defun weighted-choice ()
  "Returns the weighted choice number based on the players history."
  (* (random 1.0)
     (apply #'+ (alexandria:hash-table-values *player-history*))))

(defun computer-choice (&key (verbose t))
  "Returns the computer's choice based on what the player will likely play based on their history."
  (let ((r (weighted-choice)))
    (if verbose (format t "R: ~A : ~S~%" r (history)))
    (cond ((< r (gethash 'rock *player-history*)) ; guessing player picks rock
           'paper)
          ((< (- r (gethash 'rock *player-history*))
              (gethash 'paper *player-history*)) ; guessing player picks paper
           'scissors)
          (t 'rock)))) ; guess player picks scissors

;;; Actual game playing code.
(defun reset ()
  "Resets the score, player history, and pseudo-random state."
  (setf *random-state* (make-random-state t))
  (reset-score) 
  (reset-history))

(defun challenge (rps-choice &key (verbose t) (smart t) (keep-score t))
  "Does a game of rock-paper-scissors against the computer. Returns a rps-p value. If verbose is set, it will print a bunch of info. If smart is set, the computer will make a weighted choice and record the players choices. If keep-score is set, it will record the results."
  (let* ((computer-choice (if smart   ; if we check history
                              (computer-choice :verbose verbose)
                              (rps))) ; otherwise, random
         (r (rps-winner computer-choice rps-choice :keep-score keep-score)))
    (when verbose
      (format t "Computer chooses: ~A~%" computer-choice)
      (format t "Winner => ~A!~%" r))
    (if smart (add-history rps-choice))
    (if (and verbose keep-score) (print-score))
    r))

(defun skill-check (skill-level rps-choice &key (verbose t) (smart nil))
  "Does a Uber LARP rock-paper-scissors skill check. Returns the modified skill level. By default, :SMART is nil since this may be used by multiple people."
  (let ((r (challenge rps-choice 
                      :verbose verbose 
                      :smart smart 
                      :keep-score nil)))
    (cond ((eq r 'tie)   skill-level)
          ((eq r 'human) (+ skill-level 2))
          (t             (- skill-level 2)))))

(defun quick-check (skill-level level-to-succeed rps-choice 
                    &key (verbose nil) (smart nil))
  "Given the skill levels and a rps-choice, deterimines if you succeed of not."
  (cond ((>= (skill-check skill-level rps-choice :verbose verbose :smart smart) 
             level-to-succeed)
         (if verbose (format t "You succeed!~%"))
         t)
        (t (if verbose (format t "You fail!~%"))
           nil)))

(defun auto-check (skill-level level-to-succeed &key (verbose nil))
  "Does a quick skill check based on a random rps value."
  (let ((r (rps)))
    (if verbose (format t "You choose: ~A~%" r))
    (quick-check skill-level level-to-succeed r :verbose verbose)))

(reset) ; call reset by default

#|

This second part of the program is used to quickly generate Uber Larp 
characters with backgrounds that can be printed out via LaTeX formatting, and
that includes all of the details such as wealth and incluence cards.

|#

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Larp Rules
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def-init-class larp-character ()
  "A Uber Larp Character. Skills, feats, and flaws are added to a hash-table through add-skill or add-skills and their cousins for feats and flaws."
  ((character-set nil)
   (background nil)
   (str 0) (con 0) (dex 0)
   (int 0) (wis 0) (cha 0)
   (skills (make-hash-table))
   (feats (make-hash-table))
   (flaws (make-hash-table))
   (wealth 0)
   (influence '())
   (primary-goal "To be determined.")
   (secondary-goal "To be determined.")))

(defgeneric set-primary-goal (larp-character goal)
  (:documentation "Setter for primary-goal."))
(defmethod set-primary-goal ((c larp-character) goal)
  (setf (primary-goal c) goal))

(defgeneric set-secondary-goal (larp-character goal)
  (:documentation "Setter for secondary-goal."))
(defmethod set-secondary-goal ((c larp-character) goal)
  (setf (secondary-goal c) goal))

(defun print-items (hash)
  "Prints a hash table in the format we like for Larp characters."
  (maphash #' 
   (lambda (k v)
     (if (and (not (null v)) 
              (= v 1))
         (format t "~A~%" k)
         (format t "~A: ~A~%" k v)))
     hash))

(defmethod print-object ((c larp-character) stream)
  (format t "~A - ~A~%~%" (character-set c) (background c))
  (format t "Strength: ~A~%Constitution: ~A~%Dexterity: ~A~%" 
          (str c) (con c) (dex c))
  (format t "Intelligence: ~A~%Wisdom: ~A~%Charisma: ~A~%~%" 
          (int c) (wis c) (cha c))
  (print-items (skills c)) (terpri)
  (print-items (feats c)) (terpri)
  (print-items (flaws c))
  (format t "Wealth: ~A~%" (wealth c))
  (format t "Influence: ~A => ~S~%" 
          (length (influence c)) 
          (influence c)))

(defmacro add-item (character name type level)
  "Macro to add a skill, feat, or flaw to a character. If the item is new, it will create a new value with the specified level. Otherwise it will increase the skill by the level given."
  (let ((s (gensym)))
    `(let ((,s (gethash ,name (,type ,character))))
       (if (not ,s)
           (setf (gethash ,name (,type ,character)) ,level)
           (setf (gethash ,name (,type ,character)) (+ ,s ,level))))))

(defun add-skill (character skill &optional (level 1))
  "Wrapper for macro for adding a single skill."
  (add-item character skill skills level))

(defun add-feat (character feat &optional (level 1))
  "Wrapper for macro for adding a single feat."
  (add-item character feat feats level))

(defun add-flaw (character flaw &optional (level 1))
  "Wrapper for macro for adding a single flaw."
  (add-item character flaw flaws level))

(defun add-background (character &optional background-fun)
  "A function to add optional backgrounds to characters. This let's us mix and match backgrounds or not."
  (when background-fun
    (setf (background character) background-fun)
    (funcall background-fun character)))

(defmacro all-items (character type &body body)
  "Macro to make writing lists of items easy."
  (let ((i (gensym)))
    `(dolist (,i ,@body)
       (add-item ,character (car ,i) ,type (second ,i)))))

(defun all-skills (character &rest skill-list)
  "Skill wrapper around all-items."
  (all-items character skills (car skill-list)))

(defun all-feats (character &rest feat-list)
  "Feat wrapper around all-items."
  (all-items character feats (car feat-list)))

(defun all-flaws (character &rest flaw-list)
  "Flaw wrapper around all-items."
  (all-items character flaws (car flaw-list)))

(defgeneric has-attribute (character attribute)
  (:documentation "Returns the value of the attribute. The attribute can be a skill, feat, or flaws."))
  
(defmethod has-attribute ((character larp-character) attribute)
  (block found
    (aif (gethash attribute (skills character))
         (if it (return-from found (values it 'skills))))
    (aif (gethash attribute (feats character))
         (if it (return-from found (values it 'feats))))
    (aif (gethash attribute (flaws character))
         (if it (return-from found (values it 'flaws))))))

(defmacro remove-item (character name type)
  "Macro to remove a skill, feat, or flaw to a character."
  (let ((s (gensym)))
    `(let ((,s (gethash ,name (,type ,character))))
       (if (> ,s 1)
           (decf (gethash ,name (,type ,character)))
           (remhash ,name (,type ,character))))))

(defgeneric remove-attribute (character attribute)
  (:documentation "Removes 1 level of the attribute."))
  
(defmethod remove-attribute ((character larp-character) attribute)
  (block found
    (aif (gethash attribute (skills character))
         (if it (return-from found 
                  (remove-item character attribute skills))))
    (aif (gethash attribute (feats character))
         (if it (return-from found 
                  (remove-item character attribute feats))))
    (aif (gethash attribute (flaws character))
         (if it (return-from found 
                  (remove-item character attribute flaws))))))

(defgeneric starting-wealth (c)
  (:documentation "Determines the starting wealth of a character."))
(defmethod starting-wealth ((c larp-character))
  (setf (wealth c) 1) ; starting 1
  (aif (has-attribute c 'well-off)
       (incf (wealth c) (* it 5)))
  (aif (has-attribute c 'followers)
       (incf (wealth c) it))
  (aif (has-attribute c 'business/land-owner)
       (incf (wealth c) (* it 3)))
  (aif (has-attribute c 'money-problems)
       (incf (wealth c) (* it -1)))
  (if (< (wealth c) 0)
      (setf (wealth c) 0)))

(defun add-influence (c attribute)
  (aif (has-attribute c attribute)
       (dotimes (i it)
         (push attribute (influence c)))))

(defmacro add-influences (c &body body)
  (let ((i (gensym)))
    `(dolist (,i ,@body)
       (add-influence ,c ,i))))

(defun remove-random-list-item (c)
  (when (> (length (influence c)) 0)
    (setf (influence c) (shuffle (influence c)))
    (pop (influence c))))
  
(defun bad-reputation-removal (c)
  "Removes a random influence card for each bad-reputation."
  (aif (has-attribute c 'bad-reputation-idle-mind)
       (remove-random-list-item c))
  (aif (has-attribute c 'bad-reputation-lower)
       (remove-random-list-item c))
  (aif (has-attribute c 'bad-reputation-mercenary)
       (remove-random-list-item c))
  (aif (has-attribute c 'bad-reputation-mystic)
       (remove-random-list-item c)))

(defgeneric starting-influence (c)
  (:documentation "Determines the starting influence of a character."))
(defmethod starting-influence ((c larp-character))
  (add-influences c 
    '(interrogation 
      hypnotism
      persuasion
      bribery
      streetwise
      seduction
      high-society
      diplomacy
      intrigue
      acting
      fast-talk
      followers
      appropriate-clothing
      military-rank
      rank
      high-rank
      high-rank ; twice because doubled cards
      business/land-owner
      business/land-owner))
  (bad-reputation-removal c)) ; maybe add sort list of items?

(defgeneric update-starting (c)
  (:documentation "Determines the starting influence of a character."))
(defmethod update-starting ((c larp-character))
  (setf (wealth c) 0)
  (setf (influence c) nil)
  (starting-wealth c)
  (starting-influence c))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Primary Characters
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun make-player (&key class-name (background nil)
                      str con dex
                      int wis cha
                      skills
                      feats
                      flaws)
  "Main character creation function. We could remove more boilerplate, but I don't have time ATM and this works."
  (let ((c (make-instance 'larp-character
                           :character-set class-name
                           :str str :con con :dex dex
                           :int int :wis wis :cha cha)))
    (all-skills c skills)
    (all-feats c feats)
    (all-flaws c flaws)
    (add-background c background)
    (update-starting c)
    c))

;; this is just a template to copy and paste and fill in
(defun make-tem (&optional background-fun)
  (make-player 
   :class-name 'temp
   :background background-fun
   :str 0 :con 0 :dex 0
   :int 0 :wis 0 :cha 0
   :skills '((perception 0)
             (dodge 0)
             (kn-1 3)
             (kn-2 3)
             (kn-3 3)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '(())
   :flaws '(())))

(defun make-scientist (&optional background-fun)
  (make-player :class-name 'scientist
               :background background-fun
               :str 2 :con 2 :dex 3
               :int 4 :wis 3 :cha 3
               :skills
               '((perception 3)
                 (dodge 2)
                 (kn-scientific-theories 4)
                 (kn-scientists-of-note 4)
                 (kn-chemistry 3)
                 (kn-electricity 3)
                 (engineer-mechanical 4)
                 (hth-weapons 2)
                 (ranged-weapons 2))
               :feats '((literacy 1))
               :flaws '((tied-science 1)
                        (never-touched-a-gun 1))))

(defun make-religious-figure (&optional background-fun)
  (make-player 
   :class-name 'religious-figure
   :background background-fun
   :str 2 :con 3 :dex 2
   :int 3 :wis 3 :cha 4
   :skills '((perception 2)
             (dodge 2)
             (persuasion 3)
             (kn-theology 3)
             (kn-religious-figures 3)
             (hth-weapons 2)
             (ranged-weapons 2))
   :feats '((literacy 1)
            (followers 1))
   :flaws '((devoted-faith 1))))

(defun make-prostitute (&optional background-fun)
  (make-player 
   :class-name 'prostitute
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 3 :wis 3 :cha 4
   :skills '((perception 2)
             (dodge 2)
             (kn-carnal-acts 2)
             (kn-scandals-and-rumors 2)
             (seduction 4)
             (high-society 3)
             (acting 3)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((contact 1))
   :flaws '((devoted-weath 1)
            (money-problems 1))))

(defun make-pilot (&optional background-fun)
  (make-player 
   :class-name 'pilot 
   :background background-fun
   :str 2 :con 3 :dex 4
   :int 3 :wis 2 :cha 3
   :skills '((perception 2)
             (dodge 3)
             (piloting-dirigible 3)
             (navigation 2)
             (kn-piloting-ops 2)
             (kn-air-routes 2)
             (kn-airships 3)
             (hth-weapons 2)
             (ranged-weapons 4))
   :feats '((pilot-license 1))
   :flaws '((tied-flight 1))))

(defun make-professor (&optional background-fun)
  (make-player 
   :class-name 'professor
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 4 :wis 3 :cha 3
   :skills '((perception 3)
             (dodge 2)
             (kn-scientific-theories 4)
             (kn-scientists-of-note 3)
             (kn-of-innovations 3)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((language-ancient 1)
            (literacy 1)
            (doctorate 1))
   :flaws '((cultural-gap 1))))

(defun make-business-person (&optional background-fun)
  (make-player 
   :class-name 'business-person
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 4 :wis 3 :cha 3
   :skills '((perception 3)
             (dodge 2)
             (persuasion 2)
             (trading 3)
             (kn-economics 3)
             (kn-bureaucracy 3)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((well-off 1)
            (contact 1))
   :flaws '((dislikes-rural 1))))

(defun make-military-officer (&optional background-fun)
  (make-player 
   :class-name 'military-officer
   :background background-fun
   :str 3 :con 4 :dex 2
   :int 3 :wis 2 :cha 3
   :skills '((perception 2)
             (dodge 1)
             (persuasion 2)
             (kn-strategy 2)
             (kn-campaign-battles 2)
             (kn-military-officers 3)
             (hth-weapons 3)
             (ranged-weapons 2))
   :feats '((literacy 1)
            (military-rank 1)
            (natural-leader 1))
   :flaws '((devoted-discipline 1)
            (cultural-gap-adventure 1))))

(defun make-old-money (&optional background-fun)
  (make-player 
   :class-name 'old-money
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 3 :wis 3 :cha 4
   :skills '((perception 2)
             (dodge 2)
             (high-society 3)
             (intrigue 2)
             (kn-wealthy-hobbies 2)
             (kn-local-politics 2)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((business/land-owner 1)
            (well-off 1)
            (handy-servant 1))
   :flaws '((tied-nobility 1)
            (cultural-gap-class 1))))

(defun make-new-money (&optional background-fun)
  (make-player 
   :class-name 'new-money
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 4 :wis 3 :cha 3
   :skills '((perception 3)
             (dodge 2)
             (appraisal 3)
             (bribery 2)
             (kn-local-market 2)
             (kn-people-of-business 2)
             (kn-own-profession 3)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((well-off 1)
            (handy-servant 1))
   :flaws '((dislikes-rural 1)
            (devoted-weath 1))))

(defun make-minor-noble (&optional background-fun)
  (make-player 
   :class-name 'minor-noble
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 3 :wis 3 :cha 4
   :skills '((perception 2)
             (dodge 2)
             (high-society 3)
             (intrigue 2)
             (kn-wealthy-hobbies 2)
             (kn-other-nobles 2)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((rank 1)
            (well-off 1)
            (handy-servant 1))
   :flaws '((tied-nobility 1)
            (cultural-gap-class 1))))

(defun make-major-noble (&optional background-fun)
  (make-player 
   :class-name 'major-noble
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 3 :wis 3 :cha 4
   :skills '((perception 2)
             (dodge 2)
             (high-society 3)
             (intrigue 2)
             (kn-wealthy-hobbies 2)
             (kn-other-nobles 2)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((rank 2)
            (well-off 1)
            (handy-servant 1))
   :flaws '((tied-nobility 1)
            (cultural-gap-class 1)
            (watched-journalists 1))))

(defun make-doctor (&optional background-fun)
  (make-player 
   :class-name 'doctor
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 4 :wis 3 :cha 3
   :skills '((perception 3)
             (dodge 2)
             (first-aid 4)
             (kn-medical-theories 3)
             (kn-medicines 3)
             (kn-biology 3)
             (kn-toxins-and-poisons 3)
             (hth-weapons 2)
             (ranged-weapons 2))
   :feats '((literacy 1)
            (doctorate 1))
   :flaws '((tied-medicine 1)
            (never-touched-a-gun 1))))

(defun make-journalist (&optional background-fun)
  (make-player 
   :class-name 'journalist
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 4 :wis 3 :cha 3
   :skills '((perception 3)
             (dodge 2)
             (fast-talk 2)
             (streetwise 2)
             (kn-local-politics 3)
             (kn-local-city 3)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((literacy 1)
            (contact 1))
   :flaws '((tied-curiosity 1))))

(defun make-entertainer (&optional background-fun)
  (make-player 
   :class-name 'entertainer
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 3 :wis 3 :cha 4
   :skills '((perception 2)
             (dodge 2)
             (acting 3)
             (fast-talk 3)
             (streetwise 3)
             (kn-entertainment 2)
             (kn-tales-and-myths 2)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((contact 1))
   :flaws '((tied-attention 1))))

(defun make-explorer (&optional background-fun)
  (make-player 
   :class-name 'explorer
   :background background-fun
   :str 2 :con 4 :dex 3
   :int 3 :wis 2 :cha 3
   :skills '((perception 2)
             (dodge 2)
             (navigation 2)
             (kn-world-geography 3)
             (kn-savage-group 3)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((well-off 1)
            (handy-servant 1)
            (language-savage 1))
   :flaws '((tied-adventure 1)
            (cultural-gap-adventure 1))))

(defun make-life-of-crime (&optional background-fun)
  (make-player 
   :class-name 'life-of-crime
   :background background-fun
   :str 3 :con 2 :dex 4
   :int 3 :wis 2 :cha 3
   :skills '((perception 2)
             (dodge 3)
             (stealth 3)
             (fast-talk 2)
             (appraisal 2)
             (streetwise 2)
             (kn-local-law 2)
             (kn-criminals 2)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((contact 1))
   :flaws '((bad-reputation-lower 1)
            (watched-rival 1))))

(defun make-guild-executive (&optional background-fun)
  (make-player 
   :class-name 'guild-executive
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 4 :wis 3 :cha 3
   :skills '((perception 3)
             (dodge 2)
             (persuasion 2)
             (kn-guild-personages 3)
             (kn-trade-law 3)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((well-off 1)
            (literacy 1)
            (handy-servant 1))
   :flaws '((tied-guild-executives 1))))

(defun make-dandy (&optional background-fun)
  (make-player 
   :class-name 'dandy
   :background background-fun
   :str 2 :con 3 :dex 3
   :int 3 :wis 2 :cha 4
   :skills '((perception 2)
             (dodge 2)
             (high-society 3)
             (acting 3)
             (kn-carousing-spots 2)
             (kn-alcohol 2)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((appropriate-clothing 1)
            (well-off 1)
            (handy-servant 1))
   :flaws '((tied-nobility 1)
            (bad-reputation-idle-mind 1))))

(defun make-inventor (&optional background-fun)
  (make-player 
   :class-name 'inventor
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 4 :wis 3 :cha 3
   :skills '((perception 3)
             (dodge 2)
             (kn-scientific-theories 3)
             (kn-scientists-of-note 3)
             (kn-automatonology 3)
             (kn-electricity 3)
             (engineer-automaton 3)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((literacy 1)
            (utility-belt 1)
            (gadgeteer 1))
   :flaws '((cultural-gap-academic 1)
            (tied-science 1))))

(defun make-detective (&optional background-fun)
  (make-player 
   :class-name 'detective
   :background background-fun
   :str 2 :con 2 :dex 3
   :int 4 :wis 3 :cha 3
   :skills '((perception 4)
             (dodge 2)
             (interrogation 3)
             (forensics 3)
             (deduction 3)
             (hth-weapons 2)
             (ranged-weapons 3))
   :feats '((literacy 1)
            (contact 1))
   :flaws '((watched-rival 1)
            (tied-curiosity 1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Backgrounds
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun tmp-background (c)
  (setf (background c) 'p)
  (all-skills c '(()))
  (all-feats c '(()))
  (all-flaws c '(())))

(defun ruffian-past-background (c)
  (setf (background c) 'ruffian-past)
  (all-skills c '((kn-black-market 2)
                  (kn-other-ruffians 2)
                  (stealth 1)
                  (set-traps 1)))
  (all-feats c '((sneak-attack 1)
                 (martial-training 1)))
  (all-flaws c '((money-problems 1)
                 (tied-secret-past 1))))

(defun mercenary-or-footsoldier-background (c)
  (setf (background c) 'mercenary-or-footsoldier)
  (all-skills c '((kn-tactics 2)
                  (kn-local-armies 2)
                  (appraisal 1)))
  (all-feats c '((weapons-training 1)
                 (martial-training 1)))
  (all-flaws c '((bad-reputation-mercenary 1))))

(defun secret-dandy-background (c)
  (setf (background c) 'secret-dandy)
  (all-skills c '((kn-other-carousers 2)
                  (kn-games-of-chance 2)
                  (high-society 1)
                  (gambling 1)))
  (all-feats c '((appropriate-clothing 1)))
  (all-flaws c '((bad-reputation-idle-mind 1))))

(defun secret-society-background (c)
  (setf (background c) 'secret-society)
  (all-skills c '((kn-rival-group 2)
                  (kn-group-history 2)
                  (interrogation 1)
                  (streetwise 1)))
  (all-feats c '((access-to-secrets 1)
                 (stealth-weapon 1)))
  (all-flaws c '((watched-rival 1)
                 (devoted-cause 1))))
  
(defun street-urchin/orphan-past-background (c)
  (setf (background c) 'street-urchin/orphan-past)
  (all-skills c '((kn-scams-and-tricks 2)
                  (kn-local-city 2)
                  (pick-pocketing 1)
                  (survival-urban 1)
                  (streetwise 1)))
  (all-feats c '((alertness 1)))
  (all-flaws c '((bad-reputation-lower 1)
                 (dislikes-rural 1))))

(defun aspiring-entertainer-background (c)
  (setf (background c) 'aspiring-entertainer)
  (all-skills c '((kn-entertainment 2)
                  (kn-tales-and-myths 2)
                  (acting 1)
                  (streetwise 1)))
  (all-feats c '((contact 1)))
  (all-flaws c '((tied-attention 1))))

(defun pocket-dipper-past-background (c)
  (setf (background c) 'pocket-dipper-past)
  (all-skills c '((kn-black-market 2)
                  (kn-pocket-dippers 2)
                  (pick-pocketing 2)
                  (appraisal 1)
                  (streetwise 1)))
  (all-flaws c '((devoted-weath 1)
                 (tied-secret-past 1))))

(defun savage-from-a-distant-land-background (c)
  (setf (background c) 'savage-from-a-distant-land)
  (all-skills c '((kn-homeland 2)
                  (kn-homeland-people 2)
                  (survival-homeland-terrain 1)
                  (navigation 1)))
  (all-feats c '((alertness 1)
                 (martial-training 1)))
  (all-flaws c '((cultural-gap-savage 1)
                 (dislikes-urban 1))))

(defun roguish-past-background (c)
  (setf (background c) 'roguish-past)
  (all-skills c '((kn-black-market 2)
                  (kn-locks-and-security 2)
                  (stealth 1)
                  (pick-locks 2)
                  (disarm-traps 1)))
  (all-flaws c '((money-problems 1)
                 (tied-secret-past 1))))

(defun secret-bid-to-power-background (c)
  (setf (background c) 'secret-bid-to-power)
  (all-skills c '((kn-local-politics 2)
                  (kn-bureaucracy 2)
                  (persuasion 1)))
  (all-feats c '((followers 1)
                 (contact 1)))
  (all-flaws c '((watched-journalists 1))))

(defun dabbles-in-science-background (c)
  (setf (background c) 'dabbles-in-science)
  (all-skills c '((kn-scientific-theories 2)
                  (kn-scientists-of-note 2)
                  (kn-chemistry 1)
                  (kn-electricity 1)
                  (engineer-mechanical 1)))
  (all-feats c '((literacy 1)))
  (all-flaws c '((tied-science 1))))

(defun dabbles-in-aether-background (c)
  (setf (background c) 'dabbles-in-aether)
  (all-skills c '((kn-aether 2)
                  (kn-cryptomancers 2)))
  (all-feats c '((guide 1)
                 (special-equipment-aether-window 1)
                 (mental-awareness 1)))
  (all-flaws c '((tied-science 1))))

(defun guild-spy-background (c)
  (setf (background c) 'guild-spy)
  (all-skills c '((kn-guild-secrets 2)
                  (kn-secret-messages 2)
                  (stealth 1)
                  (fast-talk 1)
                  (interrogation 1)))
  (all-feats c '((alternate-identity 1)))
  (all-flaws c '((tied-guild-executives 1)
                 (watched-rival 1))))

(defun manipulator-background (c)
  (setf (background c) 'manipulator)
  (all-skills c '((kn-people-of-wealth 2)
                  (kn-love-techniques 2)
                  (high-society 1)
                  (seduction 1)
                  (acting 1)))
  (all-feats c '((contact 1)))
  (all-flaws c '((devoted-weath 1))))

(defun mystical-or-occult-background (c)
  (setf (background c) 'mystical-or-occult)
  (all-skills c '((kn-occult 2)
                  (kn-tricks-and-scams 2)
                  (hypnotism 2)))
  (all-feats c '((literacy 1)
                 (guide 1)))
  (all-flaws c '((tied-occult 1)
                 (bad-reputation-mystic 1))))

(defun aspiring-socialite-background (c)
  (setf (background c) 'aspiring-socialite)
  (all-skills c '((kn-style 2)
                  (kn-art-history 2)
                  (high-society 1)
                  (intrigue 1)))
  (all-feats c '((appropriate-clothing 1)))
  (all-flaws c '((never-touched-a-gun 1))))

(defun dabbles-as-detective-background (c)
  (setf (background c) 'dabbles-as-detective)
  (all-skills c '((kn-local-law 2)
                  (kn-criminals 2)
                  (interrogation 1)
                  (forensics 1)))
  (all-feats c '((literacy 1)))
  (all-flaws c '((tied-curiosity 1))))

(defun genuinely-altruistic-background (c)
  (setf (background c) 'genuinely-altruistic)
  (all-skills c '((kn-philosophy 2)
                  (kn-lower-class 2)
                  (give-council 1)
                  (streetwise 1)))
  (all-feats c '((willpower 1)))
  (all-flaws c '((devoted-cause 1))))

(defun automaton-background (c)
  (setf (background c) 'automaton)
  (all-skills c '((kn-own-race 3)
                  (language-automaton 1)
                  (babbage-engine 1))
  (all-feats c '((armored-skin 1)
                 (automaton-body 1)
                 (automaton-brain 1)
                 (special-equipment-can-hear-transmissions 1)))
  (all-flaws c '((vulnerability-electric 1)
                 (cultural-gap-robot 1)))))

(defun clockwork-cyborg-background (c)
  (setf (background c) 'clockwork-cyborg)
  (all-skills c '((kn-other-cyborgs 3)
                  (language-automaton 1)
                  (engineer-automaton 1)))
  (all-feats c '((armored-skin 1)
                 (heightened-senses 1)
                 (stealth-weapon 1)
                 (special-equipment-can-hear-transmissions 1)))
  (all-flaws c '((cultural-gap-cyborg 1)
                 (unique-appearance 1))))

;;; stock goals

(defparameter *secrets-goal*
  "Learn secrets. There are just too many opportunities to ignore. You get one point (5 max) for each secret you learn about another player character.")

(defparameter *mingle-goal*
  "Mingle. There are just too many opportunities to ignore. You get one point (5 max) for each influence you use on or receive from a different player character. Only one point per player.")

(defparameter *wealth-goal*
  "Gather wealth. There are just too many opportunities to ignore. You get one point (5 max) for each coin or coin worth of items you get through dishonest means.")

(defparameter *charity-goal* "Raise money for charity. You get one point (5 max) for every coin you raise legally, but not necessarily morally.")

(defparameter *detective-goal* "Display your deductive abilities. You achieve this goal if you discover and reveal a significant secret about another player character or a lie they have told.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Latex character sheet
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun latex-header ()
  (princ
"\\documentclass[10pt]{article}
%\\usepackage[margin=1in]{geometry} %smaller margin
\\input Zallman.fd
\\newcommand*\\initfamily{\\usefont{U}{Zallman}{xl}{n}}
\\usepackage{bookman}
\\usepackage[T1]{fontenc}

\\begin{document}
\\pagenumbering{gobble}
\\clearpage
\\thispagestyle{empty}"
;\\small"
))

(defun latex-line (&optional (size 350))
  (format t "\\line(1,0){~A}~%" size))

(defun latex-begin-mini-page (&optional (size 0.5))
  (format t "\\begin{minipage}[t]{~A\\textwidth}~%" size))

(defun latex-end-mini-page ()
  (format t "\\end{minipage}~%"))

(defun latex-title ()
  (format t "~%~%\\begin{center}~%{\\initfamily\\Huge \\\"{U}ber Larp Steampunk}~%")
  (latex-line)
  (format t "\\end{center}~%"))

(defun as-item (symbol)
  "Returns a string from a symbol with spaces where any #\- was
and with all of the first character's in the string capitalized."
  (let ((s (string symbol)))
    (setf s (cl-ppcre:regex-replace "(KN)\\-" s "Knowledge: "))
    (setf s (cl-ppcre:regex-replace "(HTH)\\-" s "Hand to Hand "))
    (format nil "~:(~A~)" (substitute #\Space #\- s))))

(defun latex-character-name (c)
  (format t "{\\Large\\bf Name:}\\\\\\\\\\\\~%")
  (format t "{\\large\\bf Character Type:}\\\\~%")
  (format t "{\\large ~A}\\\\~%" (as-item (character-set c))) 
  (format t "{\\small ~A}\\\\~%" (as-item (background c)))
  (format t "\\\\~%"))

(defun latex-character-attributes (c)
  (format t "{\\large\\bf Attributes}\\\\~%")
  (format t "Strength: ~A\\\\~%Constitution: ~A\\\\~%Dexterity: ~A\\\\~%" 
          (str c) (con c) (dex c))
  (format t "Intelligence: ~A\\\\~%Wisdom: ~A\\\\~%Charisma: ~A\\\\~%\\\\~%" 
          (int c) (wis c) (cha c)))

(defun print-latex-hash (hash)
  (maphash #' 
   (lambda (k v)
     (if (and (not (null v)) 
              (= v 1))
         (format t "~A\\\\~%" (as-item k))
         (format t "~A: ~A\\\\~%" (as-item k) v)))
     hash))

(defun latex-skills (c)
  (format t "{\\large\\bf Skills}\\\\~%")
  (print-latex-hash (skills c))
  (format t "\\\\~%"))

(defun latex-feats (c)
  (format t "{\\large\\bf Feats}\\\\~%")
  (print-latex-hash (feats c))
  (format t "\\\\~%"))

(defun latex-flaws (c)
  (format t "{\\large\\bf Flaws}\\\\~%")
  (print-latex-hash (flaws c))
  (format t "\\\\~%"))

(defun latex-others (c)
  (format t "{\\large\\bf Wealth:} {\\large~A}\\\\~%" (wealth c))
  (format t "{\\large\\bf Influence:} {\\large~A}~%" (length (influence c))))

(defun latex-goals (c)
  (format t "\\\\~%\\\\~%")
  (format t "{\\large\\bf Primary Goal:} {\\small (Worth 10)}\\\\~%")
  (format t "{\\footnotesize ~A}" (primary-goal c))
  (format t "\\\\~%\\\\~%")
  (format t "{\\large\\bf Secondary Goal:} {\\small (Worth 5)}\\\\~%")
  (format t "{\\footnotesize ~A}" (secondary-goal c)))

(defun latex-footer ()
  ;(format t "{\\large\\bf Notes:}~%")
  ;(format t "\\vfill~%\\begin{center}~%")
  ;(latex-line)
  ;(format t "\\end{center}~%")
  (format t "\\end{document}~%"))

(defun character-sheet->latex (c)
  (let ((str (make-array '(0) :element-type 'base-char
                         :fill-pointer 0 :adjustable t)))
    (with-output-to-string (*standard-output* str)
      (latex-header)
      (latex-title)
      (latex-begin-mini-page)
      (latex-character-name c)
      (latex-character-attributes c)
      (latex-skills c)
      (latex-feats c)
      (latex-end-mini-page)
      (latex-begin-mini-page)
      (latex-flaws c)
      (latex-others c)
      (latex-goals c)
      (latex-end-mini-page)
      (latex-footer))
    str))

(defun create-character-sheet (c name)
  (let ((filename (scat name ".tex")))
    (with-open-file (stream filename
                            :direction :output
                            :if-exists :supersede)
      (princ (character-sheet->latex c) stream))
    (format t "Wrote ~A~%" filename)))

(defun create-pdf (c name)
  (let* ((filename (scat name ".tex"))
         (command (scat "pdflatex " filename)))
    ;(format t "Command: ~A~%" command)
    (create-character-sheet c name)
    (cmd command) ; use popen to debug
    (format t "Wrote ~A.pdf~%" name)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; actual scenario stuff--tools to help plan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun quick-name (name c group)
  (scat (string name)
        " => "
        (string (character-set c))
        " ("
        (string (background c))
        ") ["
        (string group)
        "]"))

(defparameter *players* nil "List of players.")
(defparameter *group* nil "List of groups.")

(defun add-group (name group)
  (setf *group* (cons `(,group . ,name) *group*)))

(defun all-groups ()
  (loop for i in *group* 
     collect (car i) into x
     finally (return (remove-duplicates x :test 'equal))))

(defun in-group (group &key (latex nil))
  (loop for i in *group* 
     do (if (equal group (car i))
            (if latex
                (format t "~A\\~%" i)
                (format t "~A~%" i)))))

(defun print-groups (&key (latex nil))
  (let ((g (all-groups)))
    (assert (= (length *group*) (length *players*)) 
               nil "Player/Group Mismatch: Check groups!")
    (if latex
     (format t "~@(~R~) groups: => ~A~%~%" (length g) g)
     (format t "~@(~R~) groups: => ~A~%~%" (length g) g))
    (dolist (i g)
      (in-group i)
      (terpri))))

(defmacro defplayer (name class background group)
  `(progn
     (defparameter ,name (,class #',background))
     (push (quick-name ',name ,name ',group) *players*)
     (add-group (quick-name ',name ,name ',group) ',group)))

(defun print-game ()
  (format t "Total Player: ~A~%" (length *players*))
  ;(print-list *players*)
  (print-groups))

(defun print-character-sheets ()
  (let ((names (loop for i in *players* 
                  collect (car (split-sequence #\Space i)))))
    (dolist (i names)
        (create-pdf (symbol-value (intern i)) (string-downcase i)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Mad science
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defplayer elaine make-scientist dabbles-in-aether-background mad-science)
(set-primary-goal elaine "Protect or use the device. Since you cannot test the ``Aether Clock'' without some of the extremely rare ``Eldritch Enriched Uranium'' (EEU), you must either bring the device safely to the ``Low Country Science Convention'' or else acquire some EEU and test the device yourself. The most important thing is proving that your device works and can track the ``quantum'' like behavior of the aether, which will lead to your name going down in history!")
(set-secondary-goal elaine *secrets-goal*)

(defplayer sarah make-pilot clockwork-cyborg-background mad-science)
(set-primary-goal sarah "Protect the scientist (Elaine's character) and the device. You've been hired to insure that the scientist (worth 5 points) and the device called the ``Aether Clock'' (worth 5 points) are safely brought to the ``Low Country Science Convention''. From the Low Country, you're to fly the scientist and the device to the dark lands of Aktike.")
(set-secondary-goal sarah *secrets-goal*)

(defplayer jenny make-explorer guild-spy-background mad-science)
(set-primary-goal jenny "Protect the scientist (Elaine's character) and the device. The Guild has sent you to insure that the scientist (worth 5 points) and the device called the ``Aether Clock'' (worth 5 points) are safely brought to the ``Low Country Science Convention''. From the Low Country, the Guild wants you to accompany the scientist and the device to the dark lands of Aktike, where the Guild will ``acquire'' the device.")
(set-secondary-goal jenny *secrets-goal*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Pythagoras
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defparameter *pythogas-goal*
  "Obtain and deliver the device. As a secret member of the ``Hermetic Order of Pythagoras,'' you've been sent to help ``acquire'' the ``Aether Clock'' from the scientist (Elaine's character). The Order believes that this device can be used in conjunction with a secret ritual (which you're not privy to) to bring about a state of ``Apacostasis,'' (Platonic harmony). You must find your fellow society members who are on board (3 points). For some reason, you were never given a password to identify your fellows, so you'll have to use your wits to find them. Then, you must work with them to acquire the device and deliver it to the Order (7 points). ")

(defplayer kay make-major-noble secret-society-background pythagoras)
(set-primary-goal kay (scat *pythogas-goal* "As a senior member of the Order, you arranged to hijack the airship and deliver the device to a secret location in the aether where the ritual can be performed. You have a special device which is to be used on the pilot (an automaton) that will re-program it to fly the ship to that location."))
(set-secondary-goal kay *mingle-goal*)
                            
(defplayer doris make-old-money secret-society-background pythagoras)
(set-primary-goal doris (scat *pythogas-goal* "As a senior member of the Order, you have been tasked to hijack the airship and deliver the device to a secret location in the aether. You have hired a ruffian (Jared's character) to help you with any underhanded activity (picking locks, administering a ``knock-out'' poison, stealing items)."))
(set-secondary-goal doris *mingle-goal*)

(defplayer sharon make-new-money secret-society-background pythagoras)
(set-primary-goal sharon (scat *pythogas-goal* "As a junior member of the Order, you want to show your fellows you're worthy of promotion."))
(set-secondary-goal sharon *mingle-goal*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Good guys
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defplayer melanie make-professor genuinely-altruistic-background good-guys)
(set-primary-goal melanie "Prevent the device from being activated. You are worried that the scientist's ``Aether Clock'' is dangerous. In order for it to work, it requires a rare and dangerous substance, ``Eldritch Enriched Uranium,'' which you believe is unstable and could cause a devastating massive ``monadic black hole'' to open. This in turn could be used as a weapon. To help you, you've hired a private detective (Cameron's character) to help you investigate further and possibly ``acquire'' the device so it can't fall into the wrong hands.")
(set-secondary-goal melanie *charity-goal*)

(defplayer cameron make-detective ruffian-past-background good-guys)
(set-primary-goal cameron "Prevent the device from being activated. You've been hired by an idealistic professor (Melanie's character) to help you gather any incriminating information about some device called the ``Aether Clock,'' which the professor believes is dangerous. You achieve your goal if you can prevent the device from being used or else ``acquire'' it so that it can't fall into the wrong hands.")
(set-secondary-goal cameron *detective-goal*)

;; Wildcards
(defplayer alice make-old-money mystical-or-occult-background wild-card)
(set-primary-goal alice "Acquire mystical knowledge. Your ultimate goal is to publish a tome of occult knowledge that will secure your name in history. Although the point of your journey is to study with the ``shamen'' in the dark lands of Aktike, you're always on the look out to acquire more mystical or occult knowledge, or at least pretend to in order to gain more infamy. A specialty of yours is to con your way into an occult group or secret society and learn their secrets{\\ldots}ready for your publication. Perhaps the people that left that mysterious ``tectracys'' under your door will be on this flight.")
(set-secondary-goal alice *mingle-goal*)

(defplayer randall make-inventor secret-dandy-background wild-card)
(set-primary-goal randall "Activate the device. A scientist (Elaine's character) has invented a device called the ``Aether Clock,''  which allegedly will become the pinnacle device that unravels the quantum-like mysteries of the aether. Unfortunately, the device cannot be activated without an extremely rare and volatile substance called ``Eldritch Enriched Uranium'' (EEU). You haven't invented anything in awhile since you've been too busy carousing. But as luck would have it, you won a lot of money gambling from gent in the Rare Minerals Guild. Moreover, after you agreed to not disclose his delight in homosexual prostitutes, he was able to acquire some EEU in exchange for a settled debt and your silence. If you can help activate the device, you'll put your name back on the scientific map. Your contact said he would deliver the EEU to the ship and it is supposed to be waiting for you on-board.")
(set-secondary-goal randall *mingle-goal*)

(defplayer jared make-life-of-crime pocket-dipper-past-background wild-card)
(remove-attribute jared 'appraisal)
(remove-attribute jared 'appraisal)
(add-skill jared 'pick-locks)
(add-skill jared 'pick-locks)
(set-primary-goal jared "Gather wealth. You were hired by people who work for some hoity-toity broad to perform some illegal activities. They apparently not only wanted you for your lock picking skills, but also your ability to pilfer pockets. They also wanted you to bring a ``knock-out elixir,'' which you acquired from some shifty fellow on the black market (for 1 coin which she owes you for!). The rich woman is supposed to be on this flight, but you don't know who she is. That being said, there are just too many opportunities to ignore. You get one point (10 max) for each coin or coin worth of items you get through dishonest means.")
(set-secondary-goal jared *secrets-goal*)

(defplayer joan make-journalist dabbles-as-detective-background wild-card)
(set-primary-goal joan "Uncover a major story. You're on the trail of a developing story about a scientist (Elaine's character) who has invented a device called the ``Aether Clock,'' which allegedly will become the pinnacle device that unravels the mysteries of the aether. However an outspoken and idealistic professor (Melanie's character) believes the device is dangerous since it requires an extremely rare and volatile substance called ``Eldritch Enriched Uranium,'' which the professor believes could be used as a deadly weapon. This has led you to come up with the catchy headline ``Apocalypse Clock,'' for this device, which you're certain will raise eyebrows for everyone involved. You achieve your goal if you uncover some juicy information about the device which you can turn into a story.")
(set-secondary-goal joan *secrets-goal*)

(print-character-sheets)
(print-game)

#|

If Forest + Steph come, both wildcards.

Tied to the butler?
Doris, Randy, and Elaine.

|#
